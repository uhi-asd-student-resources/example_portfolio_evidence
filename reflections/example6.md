# Reflection on challenges faced in Monopoly game

Today, I started planning how to create the monopoly game in Javascript.  My code (LO1.1.1.5; MacDuff, 2021a) was full of data structures that depended on each other and I wanted to find a solution as it was difficult to write short unit tests for.

When I was writing the code I was trying to think of the exact syntax I wanted to write.  It was very frustrating as I seemed to take ages to write any new functionality and gaurantee that the code would still be able to run.  I was using Mocha (Mocha, 2021) to write my unit tests.

I decided to try and find source resources on the web about making my code more testable, maybe using the programming language to test my program in a variety of computation settings (LO1.1.3.5).  In the resources I used (En.wikipedia.org., 2021; GeeksforGeeks. 2021) I found out about the Model-View-Controller (MVC) architecture.  I found it hard initally to apply, and it was annoying having to tear apart my existing code.  I made the models just about the data and signalling when the data changed, using the Observer design pattern.  I added some views, which displayed my data and responded to clicks.  It took me some time to realise how to write the controller but managed it (MacDuff, 2021b).  Comparing this to my original effort was like night and day.  I can now test each class (MacDuff, 2021c) separately and I can trust that when I add a new feature the rest of the application will work.  This will help be build larger programs with more confidence in the future.

I could have asked one of the lecturers or peers to review my code and they might have pointed me in this direction earlier. In the future I will definitely use MVC architecture for my applications where appropriate.  There are some situations where it is not appropriate though that I have to be careful of (Pomberger and Press, 1994).  

GeeksforGeeks. 2021. MVC Design Pattern - GeeksforGeeks. [online] Available at: <https://www.geeksforgeeks.org/mvc-design-pattern/> [Accessed 15 February 2021].

MacDuff, 2021. [online] https://github.com/tommccallum/monopoly [Accessed 15 February 2021]

MacDuff, 2021. [online] https://github.com/tommccallum/monopoly/blob/master/controllers/BankerController.js [Accessed 15 February 2021]

MacDuff, 2021. [online] https://github.com/tommccallum/monopoly/blob/master/test/models/Banker.js [Accessed 15 February 2021]

Mochajs.org. 2021. Mocha - the fun, simple, flexible JavaScript test framework. [online] Available at: <https://mochajs.org/> [Accessed 16 February 2021].

Pomberger, G. and Pree, W., 1994, September. Quantitative and qualitative aspects of object-oriented software development. In International Symposium on Object-Oriented Methodologies and Systems (pp. 96-107). Springer, Berlin, Heidelberg.

En.wikipedia.org. 2021. Model–view–controller. [online] Available at: <https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller> [Accessed 15 February 2021].