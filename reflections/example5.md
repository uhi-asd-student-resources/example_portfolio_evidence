# Reflection on challenges faced in Monopoly game

Today, I started planning how to create the monopoly game.  My code (LO1.1.1.5; MacDuff, 2021) was like spaghetti and I wanted to find a solution as it was difficult to write short tests for.

When I was writing the code I was trying to think of the exact syntax I wanted to write.  It was very frustrating as I seemed to take ages to write any new functionality and gaurantee that the code would still be able to run.  I was using Mocha (Mocha, 2021) to write my unit tests.

I decided to try and find source resources on the web about making my code more testable (LO1.1.3.5).  In the resources I used (En.wikipedia.org., 2021; GeeksforGeeks. 2021) I found out about the Model-View-Controlr (MCV) architecture.  I found it hard initally to apply, and it was annoying having to tear apart my existing code.  I made the models just about the data and signalling when the data changed, using the Observer design pattern.  I added some views, which displayed my data and responded to clicks.  It took me some time to realise how to write the controller but managed it.  

I could have asked one of the lecturers or peers to read it and they might have pointed me in this direction earlier. In the future I will definitely use MVC architecture for my applications where appropriate. 

GeeksforGeeks. 2021. MVC Design Pattern - GeeksforGeeks. [online] Available at: <https://www.geeksforgeeks.org/mvc-design-pattern/> [Accessed 15 February 2021].

MacDuff, 2021. [online] https://github.com/tommccallum/monopoly [Accessed 15 February 2021]

Mochajs.org. 2021. Mocha - the fun, simple, flexible JavaScript test framework. [online] Available at: <https://mochajs.org/> [Accessed 16 February 2021].

En.wikipedia.org. 2021. Model–view–controller. [online] Available at: <https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller> [Accessed 15 February 2021].