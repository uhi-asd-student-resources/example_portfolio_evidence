# Reflection on challenges faced in Monopoly game

Today, I started planning how to create the monopoly game (LO1.1.1.5; 1.1.3.5).  My code (MacDuff, 2021) was like spaghetti and I wanted to find a solution as it was difficult to write short tests for.

When I was writing the code I was trying to think of the exact syntax I wanted to write.  It was very frustrating as I seemed to take ages to write any new functionality and gaurantee that the code would still be able to run.  I was using Mocha to write my unit tests.

In the resources I used (Wikipedia and GeeksforGeeks) I found out about the Model-View-Controlr (MCV) architecture.  I found it hard initally to apply, and it was annoying having to tear apart my existing code.  I made the controllers just about the data and signalling when the data changed, using the Observer design pattern.  I added some models, which displayed my data and responded to clicks.  It took me some time to realise how to write the controller but managed it.  

I could have asked for help earlier. In the future I will definitely use MVC architecture. 

MacDuff, 2021. [online] https://github.com/tommccallum/monopoly [Accessed 15 February 2021]


